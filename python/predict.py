import pickle
import scipy
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage
import os
from dnn_app_utils_v3 import *
import sys

def load_obj():
    with open(r'C:\xampp\htdocs\parkirliar\python\parameters.pkl', 'rb') as f:
        return pickle.load(f)

parameters = load_obj();

my_image = sys.argv[1] # change this to the name of your image file 
my_label_y = [1] # the true class of your image (1 -> cat, 0 -> non-cat)

fname = os.path.join('gambar/') + my_image
image = np.array(ndimage.imread(fname, flatten=False))
my_image = scipy.misc.imresize(image, size=(300,300)).reshape((300*300*3,1))
#my_image = skimage.transform.resize(image, (num_px,num_px), mode='constant').reshape((num_px*num_px*3,1))
my_image = my_image/255
my_predicted_image = predict(my_image, my_label_y, parameters)
